Package.describe({
  name: 'hsed:flow-router-helpers',
  summary: 'Template helpers for flow-router',
  git: 'https://bitbucket.org/hsedevelopment/meteor-flow-router-helpers',
  version: '0.5.2'
});

Package.onUse(function(api) {
  api.versionsFrom('2.5');

  api.use([
    'check',
    'coffeescript',
    'templating',
    'underscore',
    'hsed:active-route@2.3.4'
  ]);

  api.use([
    'kadira:flow-router@2.0.0',
    'meteorhacks:flow-router@1.19.0'
  ], ['client', 'server'], {weak: true});

  api.imply('hsed:active-route', ['client', 'server']);

  api.addFiles([
    'client/helpers.html'
  ], ['client']);

  api.addFiles([
    'client/helpers.coffee'
  ], ['client', 'server']);

  api.export('FlowRouterHelpers', 'server');
});
